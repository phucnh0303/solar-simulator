using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackgroundLoading : BaseMono
{
    [Header("Moving Background")]
    [SerializeField] private RawImage rawImage;
    [SerializeField] private Vector2 vectorOffset;

    public override void OnTick()
    {
        base.OnTick();
        var rect = rawImage.uvRect;
        rawImage.uvRect = new Rect(rect.position + vectorOffset * Time.deltaTime, rect.size);
    }
}
