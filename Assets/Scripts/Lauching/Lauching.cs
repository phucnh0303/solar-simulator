using System;
using System.Collections;
using System.Threading.Tasks;
using DG.Tweening;
using TMPro;
using Unity.Loading;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Lauching : BaseMono
{
    [Header("Logo")] [SerializeField] private Image imageLogo;
    [SerializeField] private float durationScaleLogo;
    [SerializeField] private Vector3 scale;
    [SerializeField] private GameObject background;

    [Header("Main Loading")] [SerializeField]
    private GameObject mainLoading;

    [SerializeField] private Button btnOpen;

    [Header("Progress Bar")] [SerializeField]
    private Slider progressBar;

    [SerializeField] private TextMeshProUGUI percentText;

    [Header("Notification Config")] [SerializeField]
    private NotificationConfig notificationConfig;

    private float _progressValue;

    private void Start()
    {
        Initialize();
    }

    private void Initialize()
    {
        ZoomInLogo(ShowMainLoading);
    }

    private void ZoomInLogo(Action actionCallback = null)
    {
        // Do zoom in the logo image
        imageLogo.transform.DOScale(scale, durationScaleLogo)
            .SetEase(Ease.Linear)
            .OnComplete(() =>
            {
                // Start fading for logo image
                imageLogo.DOFade(0, durationScaleLogo / 3)
                    .OnComplete(() => { DisableLogo(); actionCallback?.Invoke(); });
            });
    }

    private void DisableLogo()
    {
        imageLogo.gameObject.SetActive(false);
        background.SetActive(false);
    }

    private async void ShowMainLoading()
    {
        mainLoading.SetActive(true);
        // Start running progress bar
        await Loading(AfterProgress);
        //StartCoroutine(LoadIntoMainScene("MainMenu"));
    }

    private void AfterProgress()
    {
        progressBar.gameObject.SetActive(false);
        btnOpen.gameObject.SetActive(true);
    }

    private IEnumerator LoadIntoMainScene(string sceneName)
    {
        AsyncOperation loadOperation = SceneManager.LoadSceneAsync(sceneName);

        while (!loadOperation.isDone)
        {
            _progressValue = Mathf.Clamp01(loadOperation.progress / 0.9f);
            progressBar.value = _progressValue;
            yield return null;
        }
    }

    private async Task Loading(Action actionCallback = null)
    {
        for (int i = 0; i < progressBar.maxValue; i++)
        {
            await Task.Delay(2);
            progressBar.value = i;
        }

        actionCallback?.Invoke();
    }

    private void ScheduleNotifications()
    {
        // Setup notification
    }

    public void OnClickOpenBtn()
    {
        SceneManager.LoadSceneAsync("MainMenu");
    }
}