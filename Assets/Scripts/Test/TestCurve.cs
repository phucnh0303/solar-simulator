using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCurve : BaseMono
{
    [SerializeField] private float timeSmooth;
    [SerializeField] private GameObject movingObject;
    [SerializeField] private Transform[] controlPoints = new Transform[4];
    private Vector2 _gizmosPos;

    private void Start()
    {
        StartCoroutine(MoveObject());
    }

    private void OnDrawGizmos()
    {
        for (float t = 0; t <= 1; t += 0.05f)
        {
            _gizmosPos =
                Mathf.Pow(1 - t, 3) * controlPoints[0].position
                + 3 * Mathf.Pow(1 - t, 2) * t * controlPoints[1].position
                + 3 * (1 - t) * Mathf.Pow(t, 2) * controlPoints[2].position
                + Mathf.Pow(t, 3) * controlPoints[3].position;
            
            Gizmos.DrawWireSphere(_gizmosPos, 0.15f);
        }
    
        Gizmos.DrawLine(new Vector2(controlPoints[0].position.x, controlPoints[0].position.y), new Vector2(controlPoints[1].position.x, controlPoints[1].position.y));
        Gizmos.DrawLine(new Vector2(controlPoints[2].position.x, controlPoints[2].position.y), new Vector2(controlPoints[3].position.x, controlPoints[3].position.y));
    }

    private IEnumerator MoveObject()
    {
        var pos0 = controlPoints[0].position;
        var pos1 = controlPoints[1].position;
        var pos2 = controlPoints[2].position;
        var pos3 = controlPoints[3].position;

        float tParam = 0;
        
        while (tParam < 1)
        {
            tParam += Time.deltaTime * timeSmooth;

            movingObject.transform.position = Mathf.Pow(1 - tParam, 3) * pos0
                                 + 3 * Mathf.Pow(1 - tParam, 2) * tParam * pos1
                                 + 3 * (1 - tParam) * Mathf.Pow(tParam, 2) * pos2
                                 + Mathf.Pow(tParam, 3) * pos3;
            
            yield return new WaitForEndOfFrame();
        }

        StartCoroutine(MoveObject());
    }
}
