using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeTest : BaseMono
{
    public bool isUseUpdate;
    [SerializeField, Range(0f, 1f)] private float rotationSpeed;

    public override void OnTick()
    {
        if (!isUseUpdate)
            transform.RotateAround(transform.position, Vector3.up, rotationSpeed);
    }

    private void Update()
    {
        if (isUseUpdate)
        {
            transform.RotateAround(transform.position, Vector3.up, rotationSpeed);
        }
    }
}