public interface ICelestial
{
    public void EffectForceByOtherCelestial(ICelestial celestial1, ICelestial celestial2);
    public void CalculateTotalForce();
    public void HandleMovement();
    public void RotateAroundItself();
}
