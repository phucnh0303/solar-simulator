using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Data/Celestial")]
public class CelestialData : ScriptableObject
{
    [Header("Info")] 
    public string name;
    public string description;
    
    
    [Header("Physics")] 
    public float mass;
}
