using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseCelestial : BaseMono, ICelestial
{
    // Field area
    [SerializeField] private bool isControl;

    [Header("Force fields")] 
    [SerializeField, ConditionalShow("isControl", false)]
    private CelestialData data;

    [Header("Control")] 
    [SerializeField, ConditionalShow("isControl", true)]
    private CelestialControlData controlData;
    [SerializeField] private float radiusGravityForce;
    

    private List<BaseCelestial> _nearbyCelestials = new List<BaseCelestial>();
    private Vector3 _vel;

    // Make field be accessible area 
    public CelestialData Data => data;


    // Function area
    public override void OnTick()
    {
        base.OnTick();
        GetListCelestialsAround();
        HandleMovement();
    }

    private void GetListCelestialsAround()
    {
        _nearbyCelestials.Clear();
        var hits = Physics.OverlapSphere(transform.position, radiusGravityForce);
        foreach (var hit in hits)
        {
            var hitCelestial = hit.GetComponent<ICelestial>();
            if (hitCelestial != null)
            {
                hitCelestial.EffectForceByOtherCelestial(this, hitCelestial);
            }
        }
    }

    public virtual void EffectForceByOtherCelestial(ICelestial celestial1, ICelestial celestial2)
    {
        var forceInfo = ForceManager.CalculateGravityForce(celestial1 as BaseCelestial, celestial2 as BaseCelestial);
        Vector3 force = forceInfo.dir * forceInfo.force;

        _vel += force;
    }

    public virtual void CalculateTotalForce()
    {
        // Do move celestial with Vector3 _vel value
        
    }

    public virtual void HandleMovement()
    {
    }

    public void RotateAroundItself()
    {
        
    }
}

[Serializable]
public struct CelestialControlData
{
    public float maxSpeed;

    public float maxForce;
    // Some fields for controlling movement of the celestial
}