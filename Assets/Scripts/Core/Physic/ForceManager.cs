using System;
using System.Collections;
using System.Collections.Generic;
using Core;
using UnityEngine;

public static class ForceManager
{
    public static ForceInfo CalculateGravityForce(BaseCelestial celestial1, BaseCelestial celestial2)
    {
        float m1 = celestial1.Data.mass;
        float m2 = celestial2.Data.mass;
        Vector3 dir = celestial1.transform.position - celestial2.transform.position;
        var distance = dir.magnitude;
        var gravityForce = Constant.G * (m1 * m2) / (Mathf.Pow(distance, 2));

        return new ForceInfo(dir, gravityForce);
    }
}

[Serializable]
public struct ForceInfo
{
    public Vector3 dir;
    public float force;

    public ForceInfo(Vector3 dir, float gravityForce)
    {
        this.dir = dir;
        this.force = gravityForce;
    }
}
