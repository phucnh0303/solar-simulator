using System;
using System.Collections;
using System.Collections.Generic;
using Core;
using Unity.VisualScripting;
using UnityEngine;

[CreateAssetMenu(menuName = "Core/Ticker")]
public class Ticker : ScriptableObject
{
    [SerializeField] private TickerMono tickerMonoPrefab;

    private TickerMono _tickerMono;

    private event Action OnTick;
    private event Action OnFixedTick;
    private event Action OnLateTick;

    public void SubTick(IEntity entity)
    {
        Validate();

        OnTick += entity.OnTick;
        OnFixedTick += entity.OnFixedTick;
        OnLateTick += entity.OnLateTick;
    }

    private void Validate()
    {
        // Create ticker mono if necessary
        if (!_tickerMono)
        {
            _tickerMono = Instantiate(tickerMonoPrefab);
            _tickerMono.name = "Ticker";
            _tickerMono.Ticker = this;
        }
    }

    public void Tick()
    {
        OnTick?.Invoke();
    }

    public void FixedTick()
    {
        OnFixedTick?.Invoke();
    }

    public void LateTick()
    {
        OnLateTick?.Invoke();
    }

    public void UnSubTick(IEntity entity)
    {
        OnTick -= entity.OnTick;
        OnFixedTick -= entity.OnFixedTick;
        OnLateTick -= entity.OnLateTick;
    }
}