using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TickerMono : MonoBehaviour
{
    public Ticker Ticker { get; set; }

    private void Start()
    {
        DontDestroyOnLoad(this);
    }

    private void Update()
    {
        Ticker.Tick();
    }

    private void FixedUpdate()
    {
        Ticker.FixedTick();
    }

    private void LateUpdate()
    {
        Ticker.LateTick();   
    }
}
