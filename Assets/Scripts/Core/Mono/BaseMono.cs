using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Core;
using UnityEditor;
using UnityEngine;

public abstract class BaseMono : MonoBehaviour, IEntity
{
    [SerializeField, NamedId] string id;
    [SerializeField] private bool isUseTicker;
    [SerializeField, ConditionalShow("isUseTicker", true)] private Ticker ticker;
    
    public string Id { get => id; set => id = value; }
    
#if UNITY_EDITOR
    [ContextMenu("ResetId")]
    public void ResetId()
    {
        id = NamedIdAttributeDrawer.ToSnakeCase(name);
        EditorUtility.SetDirty(this);
    }
#endif

    private void OnEnable()
    {
        if (isUseTicker)
        {
            ticker.SubTick(this);
        }
    }

    public virtual void OnTick()
    {
    }

    public virtual void OnFixedTick()
    {
    }

    public virtual void OnLateTick()
    {
    }

    private void OnDisable()
    {
        if (isUseTicker)
        {
            ticker.UnSubTick(this);
        }
    }
    
#if UNITY_EDITOR
    void Reset()
    {
        ticker = AssetUtils.FindAssetAtFolder<Ticker>(new string[] {"Assets"}).FirstOrDefault();
        EditorUtility.SetDirty(this);
    }
#endif
}