namespace Core
{
    public interface IEntity
    {
        public void OnTick();
        public void OnFixedTick();
        public void OnLateTick();
    }
}
