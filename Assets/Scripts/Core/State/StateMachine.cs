using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

public class StateMachine
{
    public BaseState CurrentState { get; set; }
    
    public BaseState[] States { get; private set; }

    public void Initialize(params BaseState[] states)
    {
        States = states;
        foreach (var state in states)
        {
            state.StateMachine = this;
        }
    }

    public void ChangeState(BaseState state)
    {
        if(state != null && state == CurrentState) return;

        var oldState = CurrentState;
        oldState?.LeaveState();
        state?.EnterState();
    }

    #region Ticker funcitions
    /// <summary>
    /// Remember to call ticker function in object use this sateMachine
    /// </summary>
    
    public void OnStateTick()
    {
        CurrentState?.OnStateTick();
    }

    public void OnStateFixedTick()
    {
        CurrentState?.OnStateFixedTick();
    }

    public void OnStateLateTick()
    {
        CurrentState.OnStateLateTick();
    }
    
    #endregion
}