using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

public abstract class BaseState
{
    public StateMachine StateMachine { get; set; }
    public abstract void EnterState();
    public abstract void LeaveState();

    public abstract void OnState();

    public virtual void OnStateTick()
    {
        
    }

    public virtual void OnStateFixedTick()
    {
        
    }

    public virtual void OnStateLateTick()
    {
        
    }
}