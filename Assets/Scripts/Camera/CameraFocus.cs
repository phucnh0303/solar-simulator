using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public class CameraFocus : BaseMono
{
    [SerializeField] private CameraState state;
    [SerializeField] private float timeSmooth;

    private CameraStateManager _stateManager;
    private readonly Vector3[] _listPosCreateCurve = new Vector3[4];

    private void Initialize()
    {
        _stateManager = new CameraStateManager();
        _stateManager.Initialize
        (
            new CameraStateFocus(state, this),
            new CameraStateMoving(state, this)
        );
    }

    public void ChangeStateCamera(CameraState camState)
    {
        var stateScript = _stateManager.States.FirstOrDefault(baseState => baseState is BaseCameraState cameraState && cameraState.State == camState);
        _stateManager.ChangeState(stateScript);

        switch (camState)
        {   
            case CameraState.OverView:
                // Enable input handler if necessary 
                break;
            case CameraState.Moving:
                // Disable input handler if it has activated
                break;
            case CameraState.FocusCelestial:
                break;
            default:
                break;
        }
    }

    public void DoFocusOnCelestial(BaseCelestial target)
    {
        // Focus on one celestial on the universe or use cinemachine camera  
        var startPos = transform.position;
        var endPos = target.transform.position;

        StartCoroutine(MovingInCurve(startPos, endPos));
    }

    private IEnumerator MovingInCurve(Vector3 startPos, Vector3 endPos)
    {
        // Create curve from 4 pos with 2 pos is positions of camera and target celestial
        CalculatePosCurve(startPos, endPos);

        float step = 0;
        while (step < 1)
        {
            step += Time.deltaTime * timeSmooth;

            transform.position = Mathf.Pow(1 - step, 3) * _listPosCreateCurve[0]
                                 + 3 * Mathf.Pow(1 - step, 2) * step * _listPosCreateCurve[1]
                                 + 3 * (1 - step) * Mathf.Pow(step, 2) * _listPosCreateCurve[2]
                                 + Mathf.Pow(step, 3) * _listPosCreateCurve[3];

            yield return new WaitForEndOfFrame();
        }
    }

    private void CalculatePosCurve(Vector3 startPos, Vector3 endPos)
    {
        // Find 2 points that have the same distance between startPos and endPos
        _listPosCreateCurve[0] = startPos;
        _listPosCreateCurve[3] = endPos;

        var middlePos = (startPos + endPos) / 2;
        var tempDistance = Vector3.Magnitude(endPos - startPos);

        _listPosCreateCurve[1] = new Vector3(middlePos.x + tempDistance * 2, middlePos.y, middlePos.z);
        _listPosCreateCurve[2] = 2 * middlePos - _listPosCreateCurve[1];
    }
}

[Serializable]
public enum CameraState
{
    OverView,
    Moving,
    FocusCelestial,
}


#region CameraStateMachine

public class CameraStateManager : StateMachine
{
    private CameraFocus Camera { get; set; }
}

public abstract class BaseCameraState : BaseState
{
    public CameraState State;
    protected CameraFocus Camera;

    protected BaseCameraState(CameraState state, CameraFocus camera)
    {
        this.State = state;
        this.Camera = camera;
    }
}

public class CameraStateMoving : BaseCameraState
{
    public CameraStateMoving(CameraState state, CameraFocus camera) : base(state, camera)
    {
    }

    public override void EnterState()
    {
    }

    public override void LeaveState()
    {
    }

    public override void OnState()
    {
    }
}

public class CameraStateFocus : BaseCameraState
{
    public CameraStateFocus(CameraState state, CameraFocus camera) : base(state, camera)
    {
    }

    public override void EnterState()
    {
    }

    public override void LeaveState()
    {
    }

    public override void OnState()
    {
    }
}

#endregion