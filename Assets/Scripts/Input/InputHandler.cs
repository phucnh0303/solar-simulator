using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : BaseMono
{
    public override void OnTick()
    {
        base.OnTick();
        HandleInput();
    }

    private void HandleInput()
    {
        // Do something with mouse or touch
        #if UNITY_EDITOR
        
        // Handle with mouse
        if (Input.GetAxis("Mouse ScrollWheel") != 0)
        {
            // Change field of view belong to mouse scroll wheel
        }
        
        #else
        
        // Handle with touch
        
        #endif
    }
}
