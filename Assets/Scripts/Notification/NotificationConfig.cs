using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Configs/Notifications")]
public class NotificationConfig : BaseSO
{
    [SerializeField] private List<NotificationItem> listNotificationItems;
}

[Serializable]
public struct NotificationItem
{
    public int timer;
    public Sprite image;
    public string message;
}
