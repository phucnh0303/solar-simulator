using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public abstract class BasePopup : BaseMono
{
    [Header("Animation")] [SerializeField] private Transform animatedGroup;
    [SerializeField] private float animDuration;
    [SerializeField] private Ease animEase = Ease.Linear;

    public BasePopupController Controller { get; private set; }

    protected Canvas canvas;
    protected CanvasGroup canvasGroup;

    public void Initialize()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        canvas = GetComponent<Canvas>();
        animatedGroup.localScale = Vector3.zero;
    }

    public void SetOrder(int order)
    {
        canvas.sortingOrder = order;
    }

    public virtual void Show()
    {
        ShowAnim(useAnimated: true);
    }

    public virtual void Remove()
    {
        HideAnim(useAnimated: true);
    }

    private void ShowAnim(bool useAnimated = false, Action actionCallBack = null)
    {
        DOTween.Kill(this, true);
        canvasGroup.interactable = false;
        canvasGroup.blocksRaycasts = false;
        gameObject.SetActive(true);

        if (useAnimated)
        {
            DOTween.To(() => canvasGroup.alpha, x => { canvasGroup.alpha = x; }, 1, animDuration)
                .SetUpdate(true).SetTarget(this);
            animatedGroup.DOScale(Vector3.one, animDuration).SetEase(animEase).SetUpdate(true).SetTarget(this).OnComplete(
                () =>
                {
                    canvasGroup.alpha = 1;
                    animatedGroup.localScale = Vector3.one;
                    canvasGroup.interactable = true;
                    canvasGroup.blocksRaycasts = true;
                    actionCallBack?.Invoke();
                });
        }
        else
        {
            canvasGroup.alpha = 1;
            animatedGroup.localScale = Vector3.one;
            canvasGroup.interactable = true;
            canvasGroup.blocksRaycasts = true;
            actionCallBack?.Invoke();
        }
    }

    private void HideAnim(bool useAnimated = false, bool isPause = false, Action actionCallBack = null)
    {
        DOTween.Kill(this, true);

        canvasGroup.interactable = false;
        canvasGroup.blocksRaycasts = false;

        if (useAnimated)
        {
            DOTween.To(() => canvasGroup.alpha, x => { canvasGroup.alpha = x; }, 0, animDuration * 0.5f).SetUpdate(true).SetTarget(this);
            animatedGroup.DOScale(Vector3.zero, animDuration).SetTarget(this).SetUpdate(true).OnComplete(() =>
            { 
                HideCompleted(isPause);
            });
        }
        else HideCompleted(isPause);
    }

    private void HideCompleted(bool isPause, Action actionCallBack = null)
    {
        canvasGroup.alpha = 0;
        animatedGroup.localScale = isPause ? Vector3.one : Vector3.zero;
        actionCallBack?.Invoke();
    }
}