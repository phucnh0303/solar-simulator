using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Configs/Popup config")]
public class PopupConfig : BaseSO
{
    public int baseOrder;
}
