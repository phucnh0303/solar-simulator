using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomeUIController : BasePopupController
{
    private void Start()
    {
        Initialize();
        Show<TestPopup>(true);
    }
}