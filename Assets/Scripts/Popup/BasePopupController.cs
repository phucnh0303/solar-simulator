using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;

public abstract class BasePopupController : BaseMono
{
    [SerializeField] private PopupConfig popupConfig;
    [SerializeField] private BasePopup[] popupPrefabs;

    private LinkedList<BasePopup> _listPopups;
    private Dictionary<Type, BasePopup> _popupDictionary;
    private bool _isInitialized;
    private Canvas _canvas;

    public void Initialize()
    {
        if (_isInitialized) return;
        _canvas = GetComponent<Canvas>();
        _listPopups = new LinkedList<BasePopup>();
        _popupDictionary = new Dictionary<Type, BasePopup>();
    }

    private BasePopup GetPopupPrefab(Type T)
    {
        var prefab = popupPrefabs.FirstOrDefault(prefab => prefab.GetType() == T);
        return prefab;
    }

    private void AddToDictionary(Dictionary<Type, BasePopup> dictionary, BasePopup popup)
    {
        if (!dictionary.ContainsKey(popup.GetType()))
        {
            dictionary.Add(popup.GetType(), popup);
        }
    }

    private BasePopup GetBasePopup<T>()
    {
        if (!_popupDictionary.ContainsKey(typeof(T)))
        {
            var newPopup = Instantiate(GetPopupPrefab(typeof(T)), parent: this.transform);
            newPopup.Initialize();
            _popupDictionary.Add(typeof(T), newPopup);
            return newPopup;
        }

        return _popupDictionary[typeof(T)];
    }

    public void Show<T>(bool isAnimated)
    {
        var popup = GetBasePopup<T>();
        if (popup != null)
        {
            Show(isAnimated, popup);
        }
    }

    public void Show(bool isAnimated, BasePopup popup)
    {
        var topPopup = GetTopPopup();
        if (topPopup == popup) return;

        // Add popup to the top
        AddTop(popup);
        popup.Show();
    }

    private void AddTop(BasePopup popup)
    {
        if (!_listPopups.Contains(popup))
        {
            _listPopups.Remove(popup);
        }

        _listPopups.AddLast(popup);
        OnChangePopup();
    }

    private BasePopup GetTopPopup()
    {
        // Get the top popup
        return _listPopups.Last?.Value;
    }

    public void Remove()
    {
        // Remove popup
    }

    public void RemoveAll()
    {
        // Remove all popup
    }

    private void ReOrder()
    {
        // Change all sorting order of list popup 
        var baseOrder = popupConfig.baseOrder;
        var popup = _listPopups.First;
        while (popup != null)
        {
            popup.Value.SetOrder(++baseOrder);
            popup = popup.Next;
        }
    }

    private void OnChangePopup()
    {
        ReOrder();
    }
}