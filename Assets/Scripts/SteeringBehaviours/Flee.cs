using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flee : BaseSteeringObject
{
    [SerializeField] private float mass;
    [SerializeField] private float maxVelocity;
    
    private Transform _target;
    private Vector3 _currentVel;
    
    private void Initialize(Transform target)
    {
        this._target = target;
        _currentVel = Vector3.zero;
    }
    
    protected override void HandleMovement()
    {
        var vel = CalculateVelocity();
        _currentVel = (_currentVel + vel).normalized;
        transform.position += _currentVel;
    }

    private Vector3 CalculateVelocity()
    {
        var dir = (_target.position - transform.position).normalized;
        var addition = -(_currentVel - dir).normalized / mass;
        
        return addition;
    }
}
