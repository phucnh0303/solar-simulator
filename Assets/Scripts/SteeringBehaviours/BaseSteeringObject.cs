using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseSteeringObject : BaseMono
{
    protected float Mass;

    public override void OnTick()
    {
        base.OnTick();
        HandleMovement();
    }

    protected abstract void HandleMovement();
}
