using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public class Wanderer : BaseSteeringObject
{
    [SerializeField] private float maxVelocity;
    [SerializeField] private float radius;
    [SerializeField] private float timeChangeRandomPoint;
    [SerializeField, Range(0, 360)] private int deltaAngle;
    
    private Vector3 _randomPoint;
    private int _oldAngle;
    private Vector3 _currentVel;

    public void Initialize()
    {
        _oldAngle = 0;
    }

    private Vector3 CalculateRandomPoint()
    {
        var center = transform.position;
        var angle = _oldAngle + UnityEngine.Random.Range(-deltaAngle, deltaAngle);
        float newX = center.x + radius * Mathf.Cos(angle);
        float newY = center.y + radius * Mathf.Sin(angle);
        var newPos = new Vector3(newX, newY, _randomPoint.z);
        
        return newPos;
    }
    
    protected override void HandleMovement()
    {
        // Move as seeking behaviour with target position is random pos
        
        var newVel = (_currentVel + CalculateVelocity()).normalized;
        _currentVel = newVel * maxVelocity; 

        transform.position += _currentVel;
    }

    private Vector3 CalculateVelocity()
    {
        var dir = CalculateRandomPoint() - transform.position;
        var additionVel = ((dir - _currentVel)/Mass).normalized;
        
        return additionVel;
    }
}
